Download and buid a set of patches for the designed area (Puy-de-Dôme) and with production amount for each patch.

For using this script, you need to get the RPG level 2 (with farmers id, namely "Ilôts anonyme") and set correctly the [INDEX.yml](input/INDEX.yml) file for the variable ``rpg_ilots_filename``. [Cartobio](https://www.cartobio.org/) RPG dataset is also needed as listed in the index in order to assign a cultural practice to a patch.

When you run the main script [generator.py](generator.py), it will download needed data and generate a shapefile for the patches in the Puy-de-Dôme with farmers ID and production amount.

## Production data

The dataset [Cultures développées (hors fourrage, prairies, fruits, fleurs et vigne)](https://www.agreste.agriculture.gouv.fr/agreste-web/disaron/SAANR_DEVELOPPE_2/detail/) from Agreste provides the quantity producted of cereals, vegetables and wide culturals for each year and for each frend department.

By using the [visualisation tool ("tableau interactif")](https://agreste.agriculture.gouv.fr/agreste-saiku/?plugin=true&query=query/open/SAANR_DEVELOPPE_2#query/open/SAANR_DEVELOPPE_2) from Agreste, we can export dataset into the file ``SAANR_DEVELOPPE_2.csv`` by modifying the request as following:
 - add all "Cultures développées" (from 1 to 6) by clicking on them on the left panel
 - include all years (from 2000 to 2020) by clicking on "Année de référence" in the "Filtres" panel
 - add the "Puy-de-Dôme" by clicking on "Département" in the left panel

We do the same for [fruits production](https://www.agreste.agriculture.gouv.fr/agreste-web/disaron/SAANR_FRUITS/detail/) and production of [forages and meadows](https://www.agreste.agriculture.gouv.fr/agreste-web/disaron/SAANR_FOURRAGE_2/detail/), for exporting dataset in files [``SAANR_FRUITS_2.csv``](input/SAANR_FRUITS_2.csv) and [``SAANR_FOURRAGE_2.csv``](input/SAANR_FOURRAGE_2.csv).

For each dataset, culturals are classified in several hierarchical levels, from more aggregated (level 1) to more detailed (level 6).

## Correlation of RPG and SAA codes

We can read in this [report](https://www.agreste.agriculture.gouv.fr/agreste-web/download/publication/publie/Chd2002/C&D%202020-2_SAA%202018-2019%20Provisoire.pdf) (from page 44) the description of cultural codes from the SAA (annual agricultural statistics)

From the documentation of the RPG, we can found the list of cultural codes and the list of cultural group codes at page 12.

We have so established a correlation table in file ``correlation_saaniv2_rpgniv1.csv`` that map a list of codes defined for our purpose that matches with one or several codes of SAA (level 2) from one hand, and from the other hand with one or several group codes of RPG.
We have choosen the level 2 of SAA and group codes of RPG for having a suitable correlation with cultural contained in these codes.

## Usage

First, you need to get the "îlots anonymes" dataset of the RPG (here for the year 2016) that contains anonymized ID of farmers.

Then you can prepare your environment and run the generator as following:

```
pip3 install -r pip-requires.txt &&\
python3 generator.py
```
