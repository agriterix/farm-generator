import yaml
import urllib.request
import os
import zipfile
import py7zr
try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader


def download(resources_folder):
    '''
    Download and extract datafiles described in the file INDEX.yaml in the given directory.
    '''
    rf = resources_folder
    for k, v in yaml.load(open(rf + '/INDEX.yml', 'r'), Loader=Loader).items():
        if not os.path.exists(rf + '/dl'):
            os.mkdir(rf + '/dl')
        if 'url' in v:
            if 'download-filename' in v:
                dl_filename = v['download-filename']
            else:
                dl_filename = os.path.basename(v['url'])
            dl_target = rf + '/dl/' + dl_filename
            if not os.path.isfile(dl_target):
                print('Downloading {} ...'.format(dl_filename),
                      end='',
                      flush=True)
                urllib.request.urlretrieve(v['url'], dl_target)
                print('  OK')
            basename, extension = os.path.splitext(dl_target)
            if not os.path.exists(rf + '/' + v['file']):
                if extension == '.zip':
                    print('Extracting {} to {} ...'.format(
                        dl_filename, basename),
                          end='',
                          flush=True)
                    with zipfile.ZipFile(dl_target, 'r') as zip_ref:
                        zip_ref.extractall(path=basename)
                    print('  OK')
                elif extension == '.7z':
                    print('Extracting {} to {} ...'.format(
                        dl_filename, basename),
                          end='',
                          flush=True)
                    archive = py7zr.SevenZipFile(dl_target, mode="r")
                    archive.extractall(path=basename)
                    archive.close()
                    print('  OK')


if __name__ == '__main__':
    download('input')
