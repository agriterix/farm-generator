#!/usr/bin/python
# -*- coding: utf-8 -*-

import numpy as np
import pandas as pd
import geopandas as gpd
import csv
import time
from datetime import datetime


def build_studyarea(departements_filename):
    departements = gpd.GeoDataFrame.from_file(departements_filename,
                                              encoding='utf-8')
    studyarea = gpd.GeoDataFrame(
        departements[departements['CODE_DEPT'] == '63']['geometry'])
    return studyarea


class Timer:
    def __init__(self, label):
        self.new(label)

    def new(self, label):
        self.start = time.time()
        print('{} : {} … '.format(datetime.now().strftime('%Y-%m-%d %H:%M:%S'),
                                  label),
              end='',
              flush=True)

    def endAndNew(self, label):
        self.end()
        self.new(label)

    def end(self):
        elapsed = time.time() - self.start
        print('done in {:.2f}s\n'.format(elapsed))


def build_patches(studyarea, rpg_patches_filename, rpg_ilots_filename,
                  cartobio_filename):
    '''
    Return the consolidated set of patches from RPG (Registre Parcellaire Graphique, the French Land Parcel
     Identification System (LPIS))
    - rpg_ilots_filename provides farmers ID for patches but only an agregated cultural group
    - rpg_patches_filename provides the precise cultural for each patches
    - cartobio provides the organic factor for patches
    '''
    t = Timer('  load RPG patches')
    rpg = gpd.GeoDataFrame.from_file(rpg_patches_filename, encoding='utf-8')[[
        'ID_PARCEL', 'CODE_CULTU', 'geometry'
    ]]
    t.endAndNew('  sjoin with study area')
    patches = gpd.sjoin(rpg, studyarea, op='intersects')
    patches.drop(['index_right'], axis=1, inplace=True)
    t.endAndNew('  load Cartobio')
    cartobio = gpd.GeoDataFrame.from_file(
        cartobio_filename, encoding='utf-8')[['BIO', 'geometry']]
    t.endAndNew(' sjoin with study area')
    cartobio = gpd.sjoin(cartobio, studyarea, op='intersects')
    cartobio.drop(['index_right'], axis=1, inplace=True)
    t.endAndNew(' sjoin patches and cartobio')
    patches = gpd.sjoin(patches, cartobio, op='intersects')
    patches.drop(['index_right'], axis=1, inplace=True)
    t.endAndNew('  load RPG ilots')
    rpg_ilots = gpd.GeoDataFrame.from_file(
        rpg_ilots_filename,
        encoding='utf-8')[['id_ilot', 'id_expl', 'surf_ilot', 'geometry']]
    t.endAndNew('  sjoin with study area')
    ilots = gpd.sjoin(rpg_ilots, studyarea, op='intersects')
    ilots.drop(['index_right'], axis=1, inplace=True)
    t.endAndNew('  sjoin patches and ilots')
    patches_expl = gpd.sjoin(patches, ilots, how='left', op='intersects')
    t.end()
    # removing orphan patches (without exploitant)
    orphan_patches = patches_expl[patches_expl['id_ilot'].isnull()]
    print('{} patches without "id_expl" ({:.4f} % of total area)'.format(
        len(orphan_patches),
        orphan_patches.geometry.area.sum() / patches_expl.geometry.area.sum()))
    patches_expl = patches_expl[~patches_expl['id_ilot'].isnull()]
    patches_expl.geometry = patches_expl.geometry.buffer(0)
    rpg_ilots.geometry = rpg_ilots.geometry.buffer(0)
    # selecting the greatest intersection between duplicates patches from sjoin
    t.new('  processing duplicates phase 1')
    patches_expl['intersection_surf'] = patches_expl.apply(
        lambda row: rpg_ilots[rpg_ilots['id_ilot'] == row['id_ilot']][
            'geometry'].intersection(row['geometry']).area.sum(),
        axis=1)
    patches_expl['max_intersection_surf'] = patches_expl.groupby(
        'ID_PARCEL')['intersection_surf'].transform(max)
    patches_expl = patches_expl[patches_expl['max_intersection_surf'] ==
                                patches_expl['intersection_surf']]
    t.end()
    # cleaning
    del patches_expl['max_intersection_surf']
    del patches_expl['intersection_surf']
    del patches_expl['index_right']
    patches_expl['id_ilot'] = patches_expl['id_ilot'].astype(int)
    patches_expl['id_expl'] = patches_expl['id_expl'].astype(int)
    # for the last remaining duplicates, we select the "ilot" from the "exploitant" that have the greatest surface
    t.new('  processing duplicates phase 2 ')
    patches_expl['expl_surf'] = patches_expl.apply(lambda row: patches_expl[
        patches_expl['id_expl'] == row['id_expl']]['surf_ilot'].sum(),
                                                   axis=1)
    patches_expl['max_expl_surf'] = patches_expl.groupby(
        'ID_PARCEL')['expl_surf'].transform(max)
    patches_expl = patches_expl[patches_expl['max_expl_surf'] ==
                                patches_expl['expl_surf']]
    t.end()
    del patches_expl['max_expl_surf']
    del patches_expl['expl_surf']
    return patches_expl


def generate_production(patches, codes_groups_rpg, saarpg_filename,
                        saa_developpe, saa_fourrages, saa_fruits):
    # Load correlation between SAA codes and RPG codes
    code2data = {}
    with open(saarpg_filename) as csvfile:
        reader = csv.reader(csvfile, delimiter=',', quotechar='"')
        # We will read lines 2 by 2, and storing the data of the first one in this rpg variable
        rpg = None
        for row in reader:
            if not rpg:
                rpg = [c for c in row[2:] if len(c) > 0]
            else:
                saa = [c for c in row[2:] if len(c) > 0]
                code2data[row[1]] = {'saacodes': saa, 'rpgcodes': rpg}
                rpg = None
    # Prepare SAA production data
    saanr_developpe = pd.read_csv(saa_developpe, header=5, sep=";")
    saanr_fourrages = pd.read_csv(saa_fourrages, header=7, sep=";")
    saanr_fruits = pd.read_csv(saa_fruits, header=7, sep=";")
    saanr_developpe = saanr_developpe.iloc[:, [1, 2, 16]].copy()
    saanr_fourrages = saanr_fourrages.iloc[:, [1, 2, 6]].copy()
    saanr_fruits = saanr_fruits.iloc[:, [1, 2, 6]].copy()
    for df in [saanr_developpe, saanr_fourrages, saanr_fruits]:
        df.columns = ['culture2', 'culture3', 'production']
    prod_63_2016 = pd.concat([saanr_developpe, saanr_fourrages, saanr_fruits])
    prod_63_2016.dropna(subset=['culture2'],
                        inplace=True)  # remove aggregation of upper level
    prod_63_2016 = prod_63_2016[prod_63_2016['culture3'].isna()].copy(
    )  # retain only lines aggregated on level 2
    prod_63_2016.drop(['culture3'], axis=1, inplace=True)
    for code, data in code2data.items():
        data['production'] = pd.to_numeric(
            prod_63_2016[prod_63_2016['culture2'].isin(
                data['saacodes'])]['production'].apply(
                    lambda x: "".join(x.split()).replace(',', '.'))).sum()
    # Dict for getting the cultural codes for a given cultural group
    rpg_code_cultures = pd.read_csv(codes_groups_rpg,
                                    index_col=False,
                                    sep=';',
                                    encoding='latin-1')

    rpg_group2codes = rpg_code_cultures.iloc[:, [0, 3]].groupby(
        rpg_code_cultures.columns[3])[rpg_code_cultures.columns[0]].apply(
            list).to_dict()
    # Distribute production among patches
    for code, data in code2data.items():
        rpgcodes = []
        for rpggroup in data['rpgcodes']:
            rpgcodes += rpg_group2codes[rpggroup]
        myfilter = patches['CODE_CULTU'].isin(rpgcodes)
        area_sum = patches.loc[myfilter, 'geometry'].area.sum()
        patches.loc[myfilter, 'production'] = data['production'] * patches.loc[
            myfilter, 'geometry'].area / area_sum
        patches.loc[myfilter, 'codecultcorr'] = code
    return patches


if __name__ == '__main__':
    import yaml
    import download_data
    import os
    try:
        from yaml import CLoader as Loader
    except ImportError:
        from yaml import Loader
    download_data.download("input")
    if not os.path.exists("output"):
        os.mkdir("output")
    resources = {}
    config = yaml.load(open('input/INDEX.yml', 'r'), Loader=Loader)
    for k, v in config.items():
        resources[v['variable']] = 'input/' + v['file'] if 'file' in v else k
    #studyarea = gpd.GeoDataFrame.from_file(resources['study-area'], encoding='utf-8')
    studyarea = build_studyarea(resources['departements'])
    patches = build_patches(studyarea, resources['rpg_parcelles_filename'],
                            resources['rpg_ilots_filename'],
                            resources['cartobio_filename'])
    patches = generate_production(patches, resources['codes_groups_rpg'],
                                  resources['saarpg'],
                                  resources['saa_developpe'],
                                  resources['saa_fourrages'],
                                  resources['saa_fruits'])
    patches.to_file("output/patches")
